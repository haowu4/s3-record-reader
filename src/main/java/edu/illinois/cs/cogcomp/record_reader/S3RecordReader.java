package edu.illinois.cs.cogcomp.record_reader;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.util.List;

/**
 * Created by haowu on 11/3/14.
 */
public class S3RecordReader {

    private String bucketName;
    private String prefix;
    private ObjectListing currListing;
    private AmazonS3 s3;

    private ListObjectsRequest req;

    public S3RecordReader(AmazonS3 s3,String bucketName) {
        this(s3,bucketName,"");
    }

    /**
     *
     * @param s3 an Amazon S3 client,
     * @param bucketName
     * @param prefix
     */
    public S3RecordReader(AmazonS3 s3,String bucketName, String prefix) {
            this.s3=s3;
            this.bucketName = bucketName;
            this.prefix = prefix;
            this.currListing = null;
            this.connect();
    }

    /**
     * init the object listing.
     */
    private void connect(){
        this.req = new ListObjectsRequest();
        req.setBucketName(bucketName);
        req.setPrefix(prefix);
        ObjectListing rl  = this.s3.listObjects(req);
        this.currListing = rl;
        req.setMarker(rl.getNextMarker());
    }


    /**
     * Gives out 1000 records at once, until finish.
     */
    public S3RecordBatch nextBatch(){
        // If it is correctly initiated.
        if(this.currListing != null){
            ObjectListing oldListing = this.currListing;

              // If we have the next batches.
              if(this.currListing.isTruncated()){
                 List<S3ObjectSummary> l = this.currListing.getObjectSummaries();
                 this.currListing = this.s3.listObjects(req);
              }else{
                  this.currListing = null;
              }

            return new S3RecordBatch(s3,bucketName,oldListing.getObjectSummaries());

          }else{
            // Return null if no more batch to get.
            return null;
        }



    }

    public boolean hasNext(){
        return this.currListing != null;
    }


}
