package edu.illinois.cs.cogcomp.record_reader;

import edu.illinois.cs.cogcomp.thrift.curator.Record;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;

/**
 * Created by haowu on 11/3/14.
 */
public class RecordIOUtils {

    public static Record deserializeRecord(byte[] bytes) throws TException {
        Record rec = new Record();
        TDeserializer td = new TDeserializer();
        td.deserialize(rec, bytes);
        return rec;
    }
}
