package edu.illinois.cs.cogcomp.record_reader;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import edu.illinois.cs.cogcomp.thrift.curator.Record;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.thrift.TException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static edu.illinois.cs.cogcomp.record_reader.RecordIOUtils.deserializeRecord;

/**
 * Created by haowu on 11/3/14.
 */
public class S3RecordBatch {

    private List<S3ObjectSummary> s3Objects;
    private AmazonS3 s3;
    private String bucketName;

    private Iterator<S3ObjectSummary> it;

    public S3RecordBatch(AmazonS3 s3, String bucketName, List<S3ObjectSummary> s3Objects) {
        this.s3Objects = s3Objects;
        this.bucketName = bucketName;
        this.s3 = s3;
        this.it = this.s3Objects.iterator();


    }


    /**
     * This function read and deserialize all the Record binary in this batch.
     * @return
     */
    public Pair<String,Record> readNextRecords() {

        if(it.hasNext()){

            S3ObjectSummary s3object = it.next();

            String key = s3object.getKey();

            S3Object object = s3.getObject(
                    new GetObjectRequest(bucketName, key));

            InputStream objectData = object.getObjectContent();


            try {
                Record r = deserializeRecord(IOUtils.toByteArray(objectData));
                Pair<String,Record> p = new ImmutablePair<String, Record>(key,r);

                return p;

            } catch (TException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                objectData.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }else{
           return null;
        }

        return null;

    }

    public boolean hasNext(){
        return this.it.hasNext();
    }

}
