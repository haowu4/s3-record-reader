package example;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import edu.illinois.cs.cogcomp.record_reader.S3RecordBatch;
import edu.illinois.cs.cogcomp.record_reader.S3RecordReader;
import edu.illinois.cs.cogcomp.thrift.base.Labeling;
import edu.illinois.cs.cogcomp.thrift.base.Span;
import edu.illinois.cs.cogcomp.thrift.curator.Record;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by haowu on 11/3/14.
 */
public class Example {


    public static void main(String[] args) {

        AmazonS3Client s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJSBHOGAQ5M4DHH2Q", "jnttSRxpJVmWxmwBPsWcOEz+ircsf16ERlboinep"));

        //
        //  S3RecordReader reader = new S3RecordReader(s3, "BUCKET NAME", "result_coll/"+ corpora name);

        // This is the correct address for wikipedia data. (use result_coll/wiki-coref for coref data. )
        // S3RecordReader reader = new S3RecordReader(s3, "this-is-a-new-test", "result_coll/");
        S3RecordReader reader = new S3RecordReader(s3, "curator-processing-wiki", "result_coll/wiki-coref");




        // One Batch is 1000 document
        S3RecordBatch b = reader.nextBatch();

        // You can get several batches and process then in parallel.
        //        S3RecordBatch b2 = reader.nextBatch();


        // Listing is fast, but still take some time.
        System.out.println(" Finished Listing objects");

        while(b.hasNext()){

            // Read next record is time consuming because it will download things from AWS.
            Pair<String,Record> p = b.readNextRecords();

            // The string (left) is the full path in s3 of the document, which contains its document id.
            printRecord(p.getLeft(), p.getRight());

        }



    }

    public static void printRecord(String path, Record r) {
        System.out.println("Data at " + path);

        for (String k : r.getLabelViews().keySet()) {
            System.out.println("has the following labeling views " + k);
        }

        for (String k : r.getClusterViews().keySet()) {
            System.out.println("has the following cluster views " + k);
        }

        for (String k : r.getParseViews().keySet()) {
            System.out.println("has the following cluster views " + k);
        }

        if(r.getLabelViews().containsKey("wikifier")){
            System.out.println("Wikifier titles:" );
            Labeling l = r.getLabelViews().get("wikifier");
            for(Span s : l.getLabels()){
                System.out.println(s.toString());
            }
        }




    }

}
